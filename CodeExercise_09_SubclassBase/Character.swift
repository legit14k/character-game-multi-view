//
//  Character.swift
//  CodeExercise_09_SubclassBase
//
//  Created by Scott Caruso on 6/18/17.
//  Copyright © 2017 Scott Caruso. All rights reserved.
//

import Foundation

class GameCharacter {
    
    var name: String
    var level: Int
    var stats: (hitPoints: Int, magicPoints: Int, strength: Int, defense: Int, experiencePoints: Int)
    
    //This computed property determines if the user is ready to level up to the next level. It checks the character's total experience points against the number of points required for the next level, which is a separate computed property below.
    var readyToLevelUp: Bool {
        get {
            return stats.experiencePoints >= experiencePointsToLevelUp ? true : false
        }
    }
    
    //This property determines how many experience points the character needs to level up to the next level. Default implementation is 100 points per level.
    var experiencePointsToLevelUp: Int {
        get {
            return level * 100
        }
    }
    
    //The below function simulates the character winning a battle against a simple monster, adding 10 experience points for defeating it.
    func defeatMonster() {
        stats.experiencePoints += 10
        print("\(name) has defeated a green slime and gained 10 XP!")
    }
    
    //This function levels up the user, providing a random boon to each of his stats once the requisite amount of experience has been gained. The number of experience points DOES NOT change when the user levels up; he retains any points above what is necessary.
    func levelUp() -> Bool {
        switch readyToLevelUp {
        case false:
            print("The character is not ready to level up. He doesn't have enough experience points.")
            return false
        case true:
            level += 1
            stats = (stats.hitPoints + Int(arc4random_uniform(10)+1), stats.magicPoints + Int(arc4random_uniform(10)+1), stats.strength + Int(arc4random_uniform(10)+1), stats.strength + Int(arc4random_uniform(10)+1), stats.experiencePoints)
            return true
        }
    }
    
    init() {
        name = "Cecil of Baron"
        level = 1
        stats = (hitPoints: 100, magicPoints: 0, strength: 5, defense: 10, experiencePoints: 0)
    }
    
    init(name: String, level: Int, hitPoints: Int, magicPoints: Int, strength: Int, defense: Int, experiencePoints: Int) {
        self.name = name
        self.level = level
        stats = (hitPoints: hitPoints, magicPoints: magicPoints, strength: strength, defense: defense, experiencePoints: experiencePoints)
    }
}
