//
//  CharacterObjectEditor.swift
//  CodeExercise_09_SubclassBase
//
//  Created by Scott Caruso on 6/18/17.
//  Copyright © 2017 Scott Caruso. All rights reserved.
//

import UIKit

class CharacterObjectEditor: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var levelTextField: UITextField!
    @IBOutlet weak var hpTextField: UITextField!
    @IBOutlet weak var mpTextField: UITextField!
    @IBOutlet weak var strTextField: UITextField!
    @IBOutlet weak var defTextField: UITextField!
    @IBOutlet weak var xpTextField: UITextField!
    @IBOutlet weak var isImmortal: UISwitch!
    
    //var character: GameCharacter?
    var newCharacter: Subclass?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let newCharacter = newCharacter {
            nameTextField.text = String(newCharacter.name)
            ageTextField.text = String(newCharacter.age)
            levelTextField.text = String(newCharacter.level)
            hpTextField.text = String(newCharacter.stats.hitPoints)
            mpTextField.text = String(newCharacter.stats.magicPoints)
            strTextField.text = String(newCharacter.stats.strength)
            defTextField.text = String(newCharacter.stats.defense)
            xpTextField.text = String(newCharacter.stats.experiencePoints)
            isImmortal.isOn = newCharacter.isImmortal
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveChanges() {
        if (nameTextField.text?.isEmpty)! || (ageTextField.text?.isEmpty)! || (levelTextField.text?.isEmpty)! || (hpTextField.text?.isEmpty)! || (mpTextField.text?.isEmpty)! || (strTextField.text?.isEmpty)! || (defTextField.text?.isEmpty)! || (xpTextField.text?.isEmpty)! {
            displayError()
        } else {
            if let character = newCharacter, let name = nameTextField.text,let age = Int(ageTextField.text!), let level = Int(levelTextField.text!), let hp = Int(hpTextField.text!), let mp = Int(mpTextField.text!), let strength = Int(strTextField.text!), let defense = Int(defTextField.text!), let xp = Int(xpTextField.text!), let immortal = Bool?(isImmortal.isOn) {
                character.name = name
                character.age = age
                character.level = level
                character.stats.hitPoints = hp
                character.stats.magicPoints = mp
                character.stats.strength = strength
                character.stats.defense = defense
                character.stats.experiencePoints = xp
                character.immortal = immortal
                dismiss(animated: true, completion: {print("Character updated. Dismissing ViewController.")})
            } else {
                displayError()
            }
        }
    }
    
    //This function simply dismisses the ViewController without commiting any changes.
    @IBAction func dismissView() {
        dismiss(animated: true, completion: nil)
    }
    
    func displayError() {
        let alert = UIAlertController(title: "Invalid Input", message: "Please ensure there are no blanks, and all number fields have whole numbers in them!", preferredStyle: .alert)
        let okButton = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okButton)
        present(alert, animated: true, completion: nil)
    }

}
