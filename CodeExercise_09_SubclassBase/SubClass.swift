//
//  SubClass.swift
//  CodeExercise_09_SubclassBase
//
//  Created by Jason Grimberg on 5/20/18.
//  Copyright © 2018 Scott Caruso. All rights reserved.
//

import Foundation

class Subclass: GameCharacter {
    var immortal: Bool?
    var age: Int
    
    // Check to see if the character is immortal
    var isImmortal: Bool {
        get {
            if let _ = immortal {
                return true
            } else {
                return false
            }
        }
    }
    
    // Default initializer
    override init() {
        immortal = nil
        age = 18
        super.init()
    }
    
    // Initializers with new variables
    init(name: String, level: Int, hitPoints: Int, magicPoints: Int, strength: Int, defense: Int, experiencePoints: Int, immortal: Bool?, age: Int) {
        self.immortal = immortal
        self.age = age
        super.init(name: name, level: level, hitPoints: hitPoints, magicPoints: magicPoints, strength: strength, defense: defense, experiencePoints: experiencePoints)
    }
}
