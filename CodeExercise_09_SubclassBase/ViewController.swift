//
//  ViewController.swift
//  CodeExercise_09_SubclassBase
//
//  Created by Scott Caruso on 6/18/17.
//  Copyright © 2017 Scott Caruso. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var characterName: UILabel!
    @IBOutlet weak var characterLevel: UILabel!
    @IBOutlet weak var characterHP: UILabel!
    @IBOutlet weak var characterMP: UILabel!
    @IBOutlet weak var characterStrength: UILabel!
    @IBOutlet weak var characterDefense: UILabel!
    @IBOutlet weak var characterXP: UILabel!
    @IBOutlet weak var readyToLevel: UILabel!
    @IBOutlet weak var xpRequired: UILabel!
    @IBOutlet weak var characterAge: UILabel!
    @IBOutlet weak var isImmortal: UILabel!
    
    //let character = GameCharacter()
    let character = Subclass()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //Using the viewWillAppear method so that we can easily update the UI elements when coming back from the CharacterObjectEditor controller.
    override func viewWillAppear(_ animated: Bool) {
        updateLabels()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //This function calls the levelUp method, and also makes sure to tell the user if the levelUp cannot be run for some reason.
    @IBAction func levelUp() {
        let canLevel = character.levelUp()
        if !canLevel {
            let alert = UIAlertController(title: "Can't Level", message: "The character doesn't have enough XP to level up!", preferredStyle: .alert)
            let okButton = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okButton)
            present(alert, animated: true, completion: nil)
        } else {
            updateLabels()
        }
    }
    
    //Simple function to call the defeatMonster() method on the character class, increasing the character's XP.
    @IBAction func defeatMonster() {
        character.defeatMonster()
        updateLabels()
    }
    
    //This function updates the data in the text fields on demand.
    func updateLabels() {
        characterName.text = character.name
        characterAge.text = String(character.age)
        characterLevel.text = String(character.level)
        characterHP.text = String(character.stats.hitPoints)
        characterMP.text = String(character.stats.magicPoints)
        characterStrength.text = String(character.stats.strength)
        characterDefense.text = String(character.stats.defense)
        characterXP.text = String(character.stats.experiencePoints)
        readyToLevel.text = String(character.readyToLevelUp)
        xpRequired.text = String(character.experiencePointsToLevelUp)
        isImmortal.text = String(character.isImmortal)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let characterObjectEditor = segue.destination as? CharacterObjectEditor {
            characterObjectEditor.newCharacter = character
        }
    }
}

